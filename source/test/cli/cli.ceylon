import ceylon.test {
    ...
}
import org.astegiano.ceylon.cli {
    ...
}

test
shared void should_get_default_integer_value_when_no_args_are_provided() {
    value arg = IntegerArgument("test", "", 10);
    value actual = arg.getValue(["hello", "world"]);
    assertEquals(actual, 10);
}

test
shared void should_get_default_integer_value_when_arg_is_not_present() {
    value arg = IntegerArgument("test", "", 10);
    value actual = arg.getValue(["hello", "world"]);
    assertEquals(actual, 10);
}

test
shared void should_get_updated_integer_value_when_arg_is_provided() {
    value arg = IntegerArgument("size", "", 10);
    value actual = arg.getValue(["hello", "size=50"]);
    assertEquals(actual, 50);
}

test
shared void should_get_default_boolean_value_when_an_invalid_arg_is_provided() {
    value arg = BooleanArgument("test", "", false);
    value actual = arg.getValue(["test=toto", "world"]);
    assertFalse(actual);
}

test
shared void should_get_updated_boolean_value_when_true_arg_is_provided() {
    value arg = BooleanArgument("test", "", false);
    value actual = arg.getValue(["test=true", "world"]);
    assertTrue(actual);
}

test
shared void should_get_default_string_value_when_arg_is_not_present() {
    value arg = StringArgument("test", "", "bob");
    value actual = arg.getValue(["hello", "world"]);
    assertEquals(actual, "bob");
}

test
shared void should_get_updated_string_value_when_arg_is_provided() {
    value arg = StringArgument("output", "", "dir");
    value actual = arg.getValue(["hello", "output=otherdir"]);
    assertEquals(actual, "otherdir");
}
