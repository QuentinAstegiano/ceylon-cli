import org.astegiano.ceylon.cli {
    Arguments,
    BooleanArgument,
    DefaultAction,
    ArgumentValues,
    StringArgument,
    IntegerArgument
}

shared void run() {
    value args = Arguments {
            BooleanArgument {
                name = "--verbose";
                description = """Set the verbosity level of the application""";
                default = false;
            },
            StringArgument {
                name = "--output";
                description = """Set the output file""";
                default = "./build/output.file";
            },
            IntegerArgument {
                name = "--size";
                description = """Set the size of thing""";
                default = 10;
            },
            DefaultAction {
                description = """Launch the application !""";
                action = ({<String->ArgumentValues>*} config) {
                    if (exists verbose = config.find((String key->ArgumentValues val) => key == "--verbose")) {
                        print("Verbose mode is: ``verbose.pair[1]``");
                    }
                    if (exists output = config.find((String key->ArgumentValues val) => key == "--output")) {
                        print("Output to: ``output.pair[1]``");
                    }
                    if (exists size = config.find((String key->ArgumentValues val) => key == "--size")) {
                        print("Size is: ``size.pair[1]``");
                    }
                    print("Hello app !");
                    return true;
                };
            }
    };
    args.execute(process.arguments);
}