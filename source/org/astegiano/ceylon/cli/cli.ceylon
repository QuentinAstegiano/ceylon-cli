shared alias ArgumentValues => String|Integer|Boolean;
shared alias ArgumentAction => Boolean({<String->ArgumentValues>*});

shared interface Argument<out Type>
given Type of String | Integer | Boolean | Boolean({<String->ArgumentValues>*}) {
    shared formal Type getValue(String[] stringArgs);
    shared formal String getHelp();
}

shared class DefaultAction(String description, ArgumentAction action)
        satisfies Argument<ArgumentAction> {
    getValue(String[] stringArgs) => action;
    getHelp() => description;
}

shared class NamedAction(shared String name, String description, ArgumentAction action)
        satisfies Argument<ArgumentAction> {
    getValue(String[] stringArgs) => action;
    getHelp() => name + " : " + description;
}

shared class BooleanArgument(shared String name, String description, shared Boolean default)
        satisfies Argument<Boolean> {
    getHelp() => name + " : " + description;

    shared actual Boolean getValue(String[] stringArgs) {
        value match = stringArgs.find((String stringArg) => stringArg == name ||stringArg.startsWith(name + "="));
        if (exists match) {
            if (match == name + "=true") {
                return true;
            } else if (match == name + "=false") {
                return false;
            }
        }
        return default;
    }
}

shared class IntegerArgument(shared String name, String description, shared Integer default)
        satisfies Argument<Integer> {
    getHelp() => name + " : " + description;

    shared actual Integer getValue(String[] stringArgs) {
        value match = stringArgs.find((String stringArg) => stringArg == name ||stringArg.startsWith(name + "="));
        if (exists match) {
            value string = match[match.indexOf("=") + 1 .. match.size];
            if (is Integer intVal = Integer.parse(string)) {
                return intVal;
            }
        }
        return default;
    }
}

shared class StringArgument(shared String name, String description, shared String default)
        satisfies Argument<String> {
    getHelp() => name + " : " + description;

    shared actual String getValue(String[] stringArgs) {
        value match = stringArgs.find((String stringArg) => stringArg == name ||stringArg.startsWith(name + "="));
        if (exists match) {
            if (match.contains("=")) {
                return match[match.indexOf("=") + 1.. match.size];
            }
        }
        return default;
    }
}

shared class Arguments({Argument<ArgumentAction|ArgumentValues>+} args) {

    {<String->ArgumentValues>*} getArgumentsValues(String[] stringArgs) =>
        args.map((Argument<ArgumentAction|ArgumentValues> arg) =>
                if (is BooleanArgument arg) then arg
                else if (is StringArgument arg) then arg
                else if (is IntegerArgument arg) then arg
                else null)
            .coalesced
            .map((BooleanArgument|StringArgument|IntegerArgument arg) =>
                if (is BooleanArgument arg) then arg.name -> arg.getValue(stringArgs)
                else if (is StringArgument arg) then arg.name -> arg.getValue(stringArgs)
                else arg.name -> arg.getValue(stringArgs));

    shared void execute(String[] stringArgs) {
        function helpAction({<String->ArgumentValues>*} a) {
            print("Options:");
            args.each((Argument<ArgumentAction|ArgumentValues> arg) => print("\t" + arg.getHelp()));
            return false;
        }

        value action = args.sequence()
            .withLeading(NamedAction {
                name = "--help2";
                description = """Print this help message""";
                action = helpAction;
            })
            .map((Argument<ArgumentAction|ArgumentValues> arg) =>
                if (is Argument<ArgumentAction> arg) then arg
                else null)
            .coalesced
            .first;

        if (exists action) {
            action.getValue(stringArgs)(getArgumentsValues(stringArgs));
        } else {
            value usage = args.reduce {
                function accumulating(String|Argument<ArgumentAction|ArgumentValues> partial, Argument<ArgumentAction|ArgumentValues> arg) =>
                    if (is BooleanArgument arg) then "``arg.name``(``arg.default``)"
                    else if (is IntegerArgument arg) then "``arg.name``(``arg.default``)"
                    else if (is StringArgument arg) then "``arg.name``(``arg.default``)"
                    else if (is NamedAction arg) then "[``arg.name``]"
                    else "";
            };
            if (is String usage) {
                print("Usage: " + usage);
            }
        }
    }
}